// @dart=2.9

import 'package:flutter/material.dart';
import 'package:otp_auth/screen/homeScreen.dart';
//import 'package:shared_preferences/shared_preferences.dart';
// import 'screen/login.dart';
import 'package:firebase_core/firebase_core.dart';

Future <void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(value: 0),
      title: "Precisely",
      // theme: ThemeData(
      //   scaffoldBackgroundColor: Color(0xff1E1E1E)
      // ),
    );
  }
}
