import 'package:flutter/material.dart';
import '/screen/card_route/app_sample.dart';
import '/screen/card_route/loan_cal.dart';
import '/screen/card_route/resume_sub.dart';
import '/screen/card_route/sch_call.dart';

import 'dashCard.dart';

class DashCardApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<DashCardApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: DashCard(),
        ),
      ),
      routes: {
        '': (context) => DashCard(),
        '/firstCategory': (context) => ResumeSubCard(),
        '/secondCategory': (context) => ScheduleCallCard(),
        '/thirdCategory': (context) => LoanCalCard(),
        '/fourthCategory': (context) => AppSampleCard(),
      },
    );
  }
}
