import 'package:flutter/material.dart';
import 'package:easy_gradient_text/easy_gradient_text.dart';
import 'personal_screens/personal_page_1.dart';
import 'academic/Educational Info.dart';
import 'package:otp_auth/global_variable.dart' as globals;

class HomeScreen1 extends StatefulWidget {
  const HomeScreen1({Key? key, required this.value}) : super(key: key);
  final int value;

  @override
  _HomeScreen1State createState() => _HomeScreen1State();
}

class _HomeScreen1State extends State<HomeScreen1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1E1E1E),
      body: ListView(
        padding: EdgeInsets.all(25),
        children: [
          buildCard(context, widget.value),
          SizedBox(
            //Use of SizedBox
            height: 20,
          ),
          buildCard1(context, widget.value),
          SizedBox(
            //Use of SizedBox
            height: 20,
          ),
          buildCard2(context, widget.value),
          SizedBox(
            //Use of SizedBox
            height: 20,
          ),
          buildCard3(context, widget.value),
        ],
      ),
    );
  }
}

Widget buildCard(context, value) => Container(
    height: 150,
    width: 450,
    child: Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color(0xff32383E), Color(0xff17191C)],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight)),
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PersonalDetails1(
                              value: value,
                            ))
                    //Navigator.push(context, MaterialPageRoute(builder: (context) => PersonalDetails1()),
                    );
              },
              child: Container(
                child: Center(
                  child: GradientText(
                      text: "Personal Info",
                      colors: [
                        Color(0xff8D98A8),
                        Color(0xffFFFFFF),
                      ],
                      style: TextStyle(fontSize: 25.0)),
                ),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Color(0xff32383E), Color(0xff17191C)],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight)),
              ),
            ),
          ),
        ),
      ),
    ));

Widget buildCard1(context, value) => Container(
    height: 150,
    width: 450,
    child: Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color(0xff32383E), Color(0xff17191C)],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight)),
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AcademicDetails1(
                              value: value,
                            )));
              },
              child: Container(
                child: Center(
                  child: GradientText(
                      text: "Educational Info",
                      colors: [
                        Color(0xff8D98A8),
                        Color(0xffFFFFFF),
                      ],
                      style: TextStyle(fontSize: 25.0)),
                ),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Color(0xff32383E), Color(0xff17191C)],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight)),
              ),
            ),
          ),
        ),
      ),
    ));

Widget buildCard2(context, value) => Container(
      height: 150,
      width: 450,
      child: Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xff32383E), Color(0xff17191C)],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24)),
              // child: InkWell(onTap: () {
              //   Navigator.push(context, MaterialPageRoute(builder: (context) => PersonalDetails1())
              //   );
              // },
              child: Container(
                child: Center(
                  child: GradientText(
                      text: "Professional Experience",
                      colors: [
                        Color(0xff8D98A8),
                        Color(0xffFFFFFF),
                      ],
                      style: TextStyle(fontSize: 25.0)),
                ),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Color(0xff32383E), Color(0xff17191C)],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight)),
              ),
            ),
          ),
        ),
      ),
    );

Widget buildCard3(context, value) => Container(
      height: 150,
      width: 450,
      child: Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xff32383E), Color(0xff17191C)],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24)),
              // child: InkWell(onTap: () {
              //   Navigator.push(context, MaterialPageRoute(builder: (context) => PersonalDetails1())
              //   );
              // },
              child: Container(
                child: Center(
                  child: GradientText(
                      text: globals.probabilityScore.toString(),
                      colors: [
                        Color(0xff8D98A8),
                        Color(0xffFFFFFF),
                      ],
                      style: TextStyle(fontSize: 25.0)),
                ),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Color(0xff32383E), Color(0xff17191C)],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight)),
              ),
            ),
          ),
        ),
      ),
    );
