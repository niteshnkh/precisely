import 'package:flutter/material.dart';

class SubmitScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text('Resume Upload'),
        backgroundColor: Colors.grey[600],
        elevation: 10.0,
        actions: [
          IconButton(
            icon: Icon(Icons.info),
            onPressed: () {},
          ),// IconButton
          IconButton(
            icon: Icon(Icons.chat),
            onPressed: () {},
          ),// IconButton
        ],// Widget
        leading: IconButton(icon: Icon(Icons.arrow_back_ios),onPressed: (){}),
      ),// AppBar
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),// Rounded Rectangle
              padding: EdgeInsets.all(0.0),
              onPressed: () {},
              child: Ink(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Color(0xff32383E), Color(0xff17191C)],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),// Linear Gradient
                  borderRadius: BorderRadius.circular(30),
                ),//BoxDecoration
                child: Container(
                  constraints: BoxConstraints(
                    maxWidth: 150,
                    minHeight: 50,
                  ),// BoxConstraints
                  alignment: Alignment.center,
                  child: Text('Submit',style: TextStyle(color: Colors.white)),
                ),// Container
              ),
            ),
          ],
        ),
      ),
    );
  }
}

