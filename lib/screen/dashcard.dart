import 'package:flutter/material.dart';
import 'dashModel.dart';

class DashCard extends StatefulWidget {

  @override
  _DashCardState createState() => _DashCardState();
}

class _DashCardState extends State<DashCard> {
  List<DashModel> contents = [
    DashModel(
        contentPic: 'assets/resume_sub.png',
        contentTitle: 'Resume \nSubmission'),
    DashModel(
        contentPic: 'assets/sch_call.png', contentTitle: 'Schedule \nCall'),
    DashModel(
        contentPic: 'assets/percentage_cal.png',
        contentTitle: 'Loan \nCalculator'),
    DashModel(
        contentPic: 'assets/app_sample.png',
        contentTitle: 'Application \nSamples'),
  ];

  final cardCategory = [
    'firstCategory',
    'secondCategory',
    'thirdCategory',
    'fourthCategory',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1E1E1E),
      body: Center(
        child: GridView.builder(
          gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, mainAxisSpacing: 15, crossAxisSpacing: 15),
          itemBuilder: (context, position) {
            return Container(
              padding: EdgeInsets.all(15),
              // margin: EdgeInsets.all(12.0),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
                child: Container(
                  padding: EdgeInsets.all(3.0),
                  width: 150,
                  height: 250,
                  decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    gradient: LinearGradient(
                      // card background color
                        colors: [Color(0xff32383E), Color(0xff17191C)]),
                  ),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(
                          context, '/${cardCategory[position]}');
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0),
                      ),
                      child: Container(
                        width: 131.43,
                        height: 165.43,
                        decoration: ShapeDecoration(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                          gradient: LinearGradient(
                            // card background color
                              colors: [Color(0xff32383E), Color(0xff17191C)]),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(contents[position].contentPic),
                            Text(
                              contents[position].contentTitle,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14.0,
                                fontFamily: 'Circular Std',
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
          itemCount: contents.length,
        ),
      ),
    );
  }
}
