import 'package:flutter/material.dart';
//import 'package:otp_auth/main.dart';
//import 'package:flutter_gradients/flutter_gradients.dart';
//import 'package:easy_gradient_text/easy_gradient_text.dart';
import 'homeScreen1.dart';
import 'card_main.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key,required this.value}) : super(key: key);
  final int value;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  List<Widget> _widgetOptions = <Widget>[
    HomeScreen1(
      value : 0
    ),
    DashCardApp(),
    Text('Profile Screen')
  ];

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //backgroundColor: Color(0xff1E1E1E),
      appBar: AppBar(
        backgroundColor: Color(0xff1E1E1E),
        elevation: 0,
        title: Text(
            "Precisely")
      ),

      body: Container(
          child: _widgetOptions.elementAt(_selectedIndex)
      ),

      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0xff1E1E1E),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("assets/Vector.png"),
                color: Color(0xFF6A7B8B),
              ),
            label: ""
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.window),
              // icon: ImageIcon(
              //   AssetImage("assets/Vector.png"),
              //   color: Color(0xFF6A7B8B),
              // ),
            label: ""
          ),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("assets/Icon color.png"),
                color: Color(0xFF6A7B8B),
              ),
            label: ""
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTap,
        selectedFontSize: 13.0,
        unselectedFontSize: 13.0,
      ),
    );
  }
}
